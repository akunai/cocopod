#
# Be sure to run `pod lib lint NukaSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'NukaSDK'
  s.version          = '1.2.0'
  s.summary          = 'NukaSDK a native Chatbot'

  s.description      = <<-DESC
NukaSDK offers a plug&play Chabot screen for any iOS App. The SDK is private and made by Akun.ai. If you want to implement in your project just let us know at hi@akun.ai.
                       DESC

  s.homepage         = 'https://tomas-akun@bitbucket.org/akunai/nuka-sdk-ios.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Tomas Cordoba' => 'tomas@akun.ai' }
  s.source           = { :git => 'https://tomas-akun@bitbucket.org/akunai/nuka-sdk-ios.git', :tag => s.version.to_s }

  s.ios.deployment_target = '12.4'
  s.swift_version = "4.2"

  s.source_files = 'NukaSDK/Classes/**/*.{h,m,swift,storyboard,xib}'
  
  s.resource_bundles = {
    'NukaSDK' => ['NukaSDK/Classes/Resources/**/*']
  }
  
  s.frameworks = 'UIKit'
  s.dependency 'Alamofire', '~> 4.8'
  s.dependency 'AlamofireImage', '~> 3.5'
  s.dependency 'CodableAlamofire', '~> 1.1.1'
  s.dependency 'JGProgressHUD', '~> 2.0.3'
  s.dependency 'FLAnimatedImage', '~> 1.0'
  s.dependency 'YoutubePlayer-in-WKWebView', '~> 0.3.0'
  
  s.module_name = 'NukaSDK'
  
  # s.public_header_files = 'Pod/Classes/**/*.h'
end
